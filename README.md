# Helloways track extractor
Extract the GPS track from a [Helloways](https://www.helloways.com) page as a GPX file, which can then be used on your phone.

## Installation
In order to fetch the necessary Python packages, just run `pip3 install --user -r requirements.txt`.

## Usage
Just run the `extractor.py` script, with the Helloways page as a parameter.

You can also pass the output directory for the created GPX file with the `-o` option.

See `extractor.py --help` for further details.

## Documentation
* [GeoPandas](https://geopandas.org)
* [GeoJSON](https://geojson.org/)
