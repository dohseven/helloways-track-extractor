#! /usr/bin/python3

'''Extract a GPS track from a Helloways page'''

import requests
import re
import json
import os
import geopandas as gpd
from argparse import ArgumentParser, ArgumentError


def main():
    helloways_base_url = 'https://www.helloways.com'

    parser = ArgumentParser(description=__doc__)
    url_arg = parser.add_argument(
                'url',
                type=str,
                help='Helloways page to parse'
            )
    parser.add_argument(
        '-o',
        '--output',
        dest='out_dir',
        help='Output directory where GPX file is created'
    )
    args = parser.parse_args()

    # Sanity checks
    if not args.url.startswith(helloways_base_url):
        raise ArgumentError(url_arg, 'Not an Helloways URL')

    # Find the track id
    r = requests.get(args.url)
    r.raise_for_status()
    m = re.search(r'tracks/([a-f0-9]+)', r.text)
    if not m:
        raise ValueError('Failed to find track id')
    track_id = m.group(1)

    # Use Helloways API to retrieve the GeoJSON path
    r = requests.get(f'{helloways_base_url}/api/tracks/{track_id}')
    r.raise_for_status()
    json_payload = r.json()
    track_name = json_payload['name']
    geojson_data = json_payload['path']
    # Remove properties, which should not be present next to
    # a FeatureCollection according to the GeoJSON specification.
    del geojson_data['properties']
    # Add track name
    if not geojson_data['features'][0]['properties']:
        geojson_data['features'][0]['properties'] = {}
    geojson_data['features'][0]['properties']['name'] = track_name

    # Convert from GeoJSON to GPX
    file_base_name = track_name.replace(' ', '_')
    geojson_file = f'{file_base_name}.geojson'
    if args.out_dir:
        geojson_file = os.path.join(args.out_dir, geojson_file)
    gpx_file = f'{file_base_name}.gpx'
    if args.out_dir:
        gpx_file = os.path.join(args.out_dir, gpx_file)
    with open(geojson_file, 'w+') as fp:
        json.dump(geojson_data, fp=fp)
    gdf = gpd.read_file(geojson_file)
    gdf.to_file(gpx_file, driver='GPX')

    # Clean up
    os.remove(geojson_file)

    print(f'GPX file created: {gpx_file}')


if __name__ == '__main__':
    main()
